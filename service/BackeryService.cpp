#include "BackeryService.h"
#include <backeryserviceadaptor.h>

BackeryService::BackeryService(QObject *parent)
    : QObject(parent)
{
    new IBackeryServiceAdaptor(this);
    QDBusConnection dbus = QDBusConnection::sessionBus();
    dbus.registerObject("/org/manjaro/OBackeryService", this);
    dbus.registerService("org.manjaro.SBackeryService");
}

BackeryService::~BackeryService(){}

void BackeryService::testSlot()
{
    
}
