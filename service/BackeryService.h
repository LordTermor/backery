#ifndef BACKERYSERVICE_H
#define BACKERYSERVICE_H

#include <QObject>

class BackeryService : public QObject
{
    Q_OBJECT
    Q_CLASSINFO("D-Bus Interface", "org.manjaro.IBackeryService");
public:
    explicit BackeryService(QObject *parent = nullptr);
    virtual ~BackeryService();

public slots:
    void testSlot();

signals:
    void testSignal();
};

#endif // BACKERYSERVICE_H
