#include "BackeryService.h"

#include <QCoreApplication>
#include <QtDBus>

int main(int argc, char *argv[])
{
    QCoreApplication a(argc, argv);

    BackeryService s(&a);

    return a.exec();
}
