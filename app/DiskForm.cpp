#include "DiskForm.h"
#include "ui_DiskForm.h"

#include <lib/ScheduleModel.h>
#include <lib/Snapshot.h>
#include <lib/SnapshotsModel.h>

#include <QDateTime>
#include <QFile>
#include <QFileDialog>
#include <QSettings>

#include <QComboBox>
#include <QSpinBox>
#include <QLabel>
#include <QRadioButton>


QString systemName(){
    QFile lsbRelease("/etc/lsb-release");
    if(lsbRelease.open(QFile::ReadOnly)){
        QMap<QString, QString> lsbReleaseMap;
        while(!lsbRelease.atEnd()){
            auto line = lsbRelease.readLine();

            auto keyvalue = line.split('=');
            if(keyvalue.size() == 2){
                lsbReleaseMap.insert(keyvalue.first().trimmed(), keyvalue.last().trimmed());
            }

        }

        auto desc = lsbReleaseMap.contains("DISTRIB_DESCRIPTION")? lsbReleaseMap["DISTRIB_DESCRIPTION"].remove("\""):"Unknown";
        auto releaes =lsbReleaseMap.contains("DISTRIB_RELEASE")?lsbReleaseMap["DISTRIB_RELEASE"]:"";
        return QString("%1 %2").arg(desc,releaes);
    }

    return "Unknown";
}


DiskForm::DiskForm(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::DiskForm)

{
    ui->setupUi(this);

    connect(this, &DiskForm::diskChanged, this, &DiskForm::updateUi);

    ui->tableView->setModel(new Backery::SnapshotsModel(this));
    ui->tableView->setSelectionBehavior(QTableView::SelectRows);
    ui->tableView_2->setModel(new Backery::ScheduleModel(this));
    ui->tableView_2->setSelectionBehavior(QTableView::SelectRows);

    ui->tableView->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->tableView->horizontalHeader()->setSectionResizeMode(1, QHeaderView::ResizeToContents);
    ui->tableView->horizontalHeader()->setSectionResizeMode(2, QHeaderView::Stretch);
    ui->tableView->horizontalHeader()->setSectionResizeMode(3, QHeaderView::ResizeToContents);
    ui->tableView->verticalHeader()->hide();


    ui->tableView_2->horizontalHeader()->setSectionResizeMode(0, QHeaderView::ResizeToContents);
    ui->tableView_2->horizontalHeader()->setSectionResizeMode(1, QHeaderView::Stretch);
    ui->tableView_2->horizontalHeader()->setSectionResizeMode(2, QHeaderView::ResizeToContents);
    ui->tableView_2->verticalHeader()->hide();

    settings = new QSettings(QSettings::SystemScope, this);
}

DiskForm::~DiskForm()
{
    delete ui;
}

void DiskForm::on_pushButton_5_clicked()
{
    settings->remove(m_disk.uuid());
    settings->sync();
    updateUi();
}

QString getLabel(const Backery::Disk& disk){

    QString fs = disk.storageInfo().fileSystemType();
    QString label = disk.storageInfo().name();
    QString device = disk.storageInfo().device();
    QString size = QLocale(QLocale::English).formattedDataSize(disk.storageInfo().bytesTotal());
    auto fsSnapshotAvailable = QStringList({"btrfs", "zfs"}).contains(fs);

    if(label.length()!=0){

        return QString("<html><head/><body>"
                            "<p><span style=\" font-size:20pt;\">%0</span> (%4)</p>"
                            "<p><span style=\" font-style:italic;\">%1</span></p>"
                            "<p><span style=\" font-weight:600;\">%2</span> %3</p>"
                            "</body></html>")
                .arg(label, device, fs.toUpper(), fsSnapshotAvailable?"(filesystem snapshots available)":"", size);
    } else {
        return QString("<html><head/><body>"
                            "<p><span style=\" font-size:20pt;\">%1</span> (%4)</p>"
                            "<p><span style=\" font-weight:600;\">%2</span> %3</p>"
                            "</body></html>")
                .arg(device, fs.toUpper(), fsSnapshotAvailable?"(filesystem snapshots available)":"", size);
    }

}

void DiskForm::updateUi()
{
    ui->tableView   -> setModel( new Backery::SnapshotsModel(this) );
    ui->tableView_2 -> setModel( new Backery::ScheduleModel( this) );

    ui->label_2->setText(getLabel(m_disk));

    if( !settings->childGroups().contains(m_disk.uuid()) ) return;

    Backery::ScheduleModel *m = (Backery::ScheduleModel*)ui->tableView_2->model();

    settings->beginGroup( m_disk.uuid() );

    ui->backup_storage->setText( settings->value("storage").toString() );

    int size = settings->beginReadArray("tasks");
    for (int i = 0; i < size; ++i) {
        settings->setArrayIndex(i);
        Backery::Task task;
        task.triggerType = (Backery::Task::Type)settings->value("triggerType").toInt();
        task.triggerStep = settings->value("triggerStep").toUInt();
        task.fsSnapshot  = settings->value("fsSnapshot").toBool();

        m->addTask( task );
    }

    settings->endArray();
    settings->endGroup();
}

Backery::Disk DiskForm::disk() const
{
    return m_disk;
}

void DiskForm::setDisk(const Backery::Disk& newDisk)
{
    m_disk = newDisk;
    emit diskChanged();
}

void DiskForm::on_pushButton_clicked()
{
    QFileDialog f( this, tr("Snapshots location") );
    f.setFileMode(QFileDialog::Directory);
    if( f.exec() ){
        QDir location = QFileDialog::getExistingDirectory( this,  tr("Snapshots location") );
        settings->setValue(m_disk.uuid()+"/storage", QVariant(location.absolutePath()) );
        ui->backup_storage->setText( location.path() );
        settings->sync();
    }
}


void DiskForm::on_pushButton_3_clicked()
{
    QDialog sheduleSelection(this);
    QGridLayout layout(this);

    QLabel label(this);
    label.setText("Every");

    QSpinBox triggerStep(this);
    triggerStep.setMinimum(1);

    QComboBox sheduleType(this);
    sheduleType.insertItems(0, {"Hour(s)", "Day(s)", "Week(s)", "Month(s)", "Year(s)"});

    QRadioButton rsyncSnap(this);
    QRadioButton fsSnap(this);
    rsyncSnap.setText("rsync");
    rsyncSnap.setChecked(true);
    fsSnap.setText("filesystem");

    QString fsName = m_disk.storageInfo().fileSystemType();
    if( fsName != "btrfs" && fsName != "zfs" ){
        rsyncSnap.setEnabled(false);
        fsSnap.setEnabled(false);
    }

    QPushButton ok("ok", &sheduleSelection);
    ok.setDefault(true);

    connect(&ok, &QPushButton::clicked, &sheduleSelection, &QDialog::accept);

    layout.addWidget(&label, 0, 0);
    layout.addWidget(&triggerStep, 0, 1);
    layout.addWidget(&sheduleType, 0, 2);
    layout.addWidget( new QLabel("Snapshot type:", this), 0, 3 );

    QVBoxLayout btns(this);
    btns.addWidget(&fsSnap);
    btns.addWidget(&rsyncSnap);
    layout.addLayout( &btns, 0, 4 );

    layout.addWidget(&ok, 1, 4);

    sheduleSelection.setLayout(&layout);

    if( sheduleSelection.exec() ){

        Backery::ScheduleModel *m = (Backery::ScheduleModel*)ui->tableView_2->model();

        Backery::Task newTask;
        newTask.triggerType = (Backery::Task::Type)sheduleType.currentIndex();
        newTask.triggerStep = triggerStep.value();
        newTask.fsSnapshot  = fsSnap.isChecked();

        settings->beginGroup( m_disk.uuid() );

        QVector<Backery::Task> taskList;

        int size = settings->beginReadArray("tasks");
        for (int i = 0; i < size; ++i) {
            settings->setArrayIndex(i);
            Backery::Task task;
            task.triggerType = (Backery::Task::Type)settings->value("triggerType").toInt();
            task.triggerStep = settings->value("triggerStep").toUInt();
            task.fsSnapshot  = settings->value("fsSnapshot").toBool();

            taskList.append(task);

            if(task == newTask){
                // msg: already exists!

                settings->endArray();
                settings->endGroup();
                return;
            }
        }
        settings->endArray();

        taskList.append( newTask );
        m->addTask(newTask);

        settings->remove("tasks");
        settings->beginWriteArray("tasks", taskList.length());
        for (int i = 0; i < taskList.length(); ++i) {
            settings->setArrayIndex(i);
            settings->setValue( "triggerType", taskList[i].triggerType );
            settings->setValue( "triggerStep", taskList[i].triggerStep );
            settings->setValue( "fsSnapshot",  taskList[i].fsSnapshot  );
        }
        settings->endArray();

        settings->endGroup();
        settings->sync();
    }
}

void DiskForm::on_pushButton_2_clicked()
{
    Backery::ScheduleModel *m = (Backery::ScheduleModel*)ui->tableView_2->model();

    settings->beginGroup(m_disk.uuid());

    QVector<Backery::Task> taskList;

    int size = settings->beginReadArray("tasks");
    for (int i = 0; i < size; ++i) {
        settings->setArrayIndex(i);
        Backery::Task task;
        task.triggerType = (Backery::Task::Type)settings->value("triggerType").toInt();
        task.triggerStep = settings->value("triggerStep").toUInt();
        task.fsSnapshot  = settings->value("fsSnapshot").toBool();

        taskList.append(task);
    }

    settings->endArray();

    taskList.removeOne( m->taskAt( ui->tableView_2->currentIndex().row() ) );

    settings->remove("tasks");

    settings->beginWriteArray("tasks");
    for (int i = 0; i < taskList.size(); ++i) {
        settings->setArrayIndex(i);
        settings->setValue( "triggerType", taskList[i].triggerType );
        settings->setValue( "triggerStep", taskList[i].triggerStep );
        settings->setValue( "fsSnapshot",  taskList[i].fsSnapshot  );
    }
    settings->endArray();
    settings->endGroup();

    settings->sync();
    updateUi();
}

