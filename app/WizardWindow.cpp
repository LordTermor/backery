#include "WizardWindow.h"
#include "ui_WizardWindow.h"
#include <lib/DiskModel.h>
#include "DiskForm.h"

#include <QStorageInfo>
#include <QCheckBox>
#include <QFileSystemModel>

void WizardWindow::updateDisksList(const QList<QStorageInfo>& infos, Ui::WizardWindow* ui)
{
    for(const auto& info : qAsConst(infos))
    {
        if (!info.isValid() || !info.isReady() || info.isReadOnly() || QStringList({"tmpfs", "run", "vfat"}).contains(info.fileSystemType())) continue;

        qobject_cast<Backery::DiskModel*>(ui->disksSelectList->model())->addDisk(info);
    }
}


WizardWindow::WizardWindow(QWidget *parent) :
    QWizard(parent),
    ui(new Ui::WizardWindow)
{
    ui->setupUi(this);


    // Disks Select
    ui->disksSelectList->setModel(new Backery::DiskModel(this));
    ui->disksSelectList->setSelectionMode(QAbstractItemView::MultiSelection);

    auto last = std::unique(infos.begin(), infos.end(), [](const QStorageInfo &a, const QStorageInfo &b)
    {
        return a.device() == b.device();
    });
    infos.erase(last, infos.end());

    updateDisksList(infos, ui);

    ui->donePageDetailedSelectLabelTitle->hide();
    ui->donePageDetailedSelectLabel->hide();

    connect(this->button(QWizard::FinishButton),
    SIGNAL(clicked()),this,SLOT(onFinishButtonCliked()));
}


WizardWindow::~WizardWindow()
{
    delete ui;
}


// Disks Select
void WizardWindow::handleDisksSelect()
{
    m_disks = ui->disksSelectList->selectionModel()->selectedIndexes();
    QString m_disks_string = "";

    for (int i = 0; i < m_disks.length(); i++)
    {
        m_disks_string += m_disks[i].data().toString() + "\n";
    }

    ui->donePageDisksSelectLabel->setText(m_disks_string);
}


// Type Options
void WizardWindow::on_typeOptionsRsyncCheck_toggled(bool checked)
{
    if (checked) m_type = "RSYNC";
}


void WizardWindow::on_typeOptionsSnapshotCheck_toggled(bool checked)
{
    if (checked) m_type = "Snapshot";
}


void WizardWindow::handleTypeOptions()
{
    ui->donePageTypeOptionsLabel->setText(m_type);
}


// Time Select
void WizardWindow::on_timeSelectMonthlyCheck_stateChanged(int arg1)
{
    if (arg1) m_timeOptions.append("Monthly");
    else m_timeOptions.removeAll("Monthly");
}


void WizardWindow::on_timeSelectWeeklyCheck_stateChanged(int arg1)
{
    if (arg1) m_timeOptions.append("Weekly");
    else m_timeOptions.removeAll("Weekly");
}


void WizardWindow::on_timeSelectDailyCheck_stateChanged(int arg1)
{
    if (arg1) m_timeOptions.append("Daily");
    else m_timeOptions.removeAll("Daily");
}


void WizardWindow::on_timeSelectHourlyCheck_stateChanged(int arg1)
{
    if (arg1) m_timeOptions.append("Hourly");
    else m_timeOptions.removeAll("Hourly");
}


void WizardWindow::handleTimeSelect()
{
    QString m_timeOptionsList_string = "";

    for (int i = 0; i < m_timeOptions.length(); i++)
    {
        m_timeOptionsList_string += m_timeOptions[i] + "\n";
    }
    ui->donePageTimeSelectLabel->setText(m_timeOptionsList_string);
}


// Slide change handler
void WizardWindow::on_WizardWindow_currentIdChanged(int id)
{
    switch (id)
    {
        case 1:
            if (ui->disksSelectList->selectionModel()->selectedIndexes().isEmpty()) WizardWindow::back();
            else handleDisksSelect();
            break;
        case 2:
            if (m_type.isEmpty()) WizardWindow::back();
            else handleTypeOptions();
            break;
        case 3:
            if (m_timeOptions.isEmpty()) WizardWindow::back();
            else handleTimeSelect();
            break;
    }
}


std::tuple<QModelIndexList, QString, QList<QString>> WizardWindow::onFinishButtonClicked()
{
     return {m_disks, m_type, m_timeOptions};
}
