#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include "DiskForm.h"
#include "WizardWindow.h"

#include <QMainWindow>
#include <QSettings>
#include "BackeryServiceInterface.h"

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();

private slots:
    void on_pushButton_4_clicked();

    void on_listView_activated(const QModelIndex &index);

    void on_actionWizard_triggered();

private:
    org::manjaro::IBackeryService *service;
    DiskForm* m_diskForm;
    Ui::MainWindow *ui;
    QSettings *settings;
};
#endif // MAINWINDOW_H
