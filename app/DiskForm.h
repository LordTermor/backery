#ifndef DISKFORM_H
#define DISKFORM_H

#include <lib/Disk.h>

#include <QWidget>
#include <QSettings>

namespace Ui {
class DiskForm;
}

class DiskForm : public QWidget
{
    Q_OBJECT

public:
    explicit DiskForm(QWidget *parent = nullptr);
    ~DiskForm();

    Backery::Disk disk() const;
    void setDisk(const Backery::Disk& newDisk);

signals:
    void diskChanged();

private slots:
    void on_pushButton_3_clicked();
    void on_pushButton_5_clicked();
    void updateUi();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Backery::Disk m_disk;

    Ui::DiskForm *ui;
    Q_PROPERTY(Backery::Disk disk READ disk WRITE setDisk NOTIFY diskChanged)

    QSettings *settings;
};

#endif // DISKFORM_H
