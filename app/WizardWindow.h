#ifndef WIZARDWINDOW_H
#define WIZARDWINDOW_H

#include "DiskForm.h"

#include <QWizard>
#include <QFileSystemModel>

namespace Ui {
class WizardWindow;
}

class WizardWindow : public QWizard
{
    Q_OBJECT

public:
    explicit WizardWindow(QWidget *parent = nullptr);
    ~WizardWindow();

    QModelIndexList getDisks();
    QString getType();
    QList<QString> getTimeOptions();

public slots:
    std::tuple<QModelIndexList, QString, QList<QString>> onFinishButtonClicked();

private slots:
    void on_typeOptionsRsyncCheck_toggled(bool checked);

    void on_typeOptionsSnapshotCheck_toggled(bool checked);

    void on_timeSelectMonthlyCheck_stateChanged(int arg1);

    void on_timeSelectWeeklyCheck_stateChanged(int arg1);

    void on_timeSelectDailyCheck_stateChanged(int arg1);

    void on_timeSelectHourlyCheck_stateChanged(int arg1);

    void on_WizardWindow_currentIdChanged(int id);

private:
    Ui::WizardWindow *ui;
    QList<QStorageInfo> infos = QStorageInfo::mountedVolumes();
    QModelIndexList m_disks;
    QString m_type;
    QList<QString> m_timeOptions;

    void updateDisksList(const QList<QStorageInfo>& infos, Ui::WizardWindow* ui);
    void handleDisksSelect();
    void handleTypeOptions();
    void handleTimeSelect();
};

#endif // WIZARDWINDOW_H
