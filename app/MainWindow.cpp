#include "MainWindow.h"
#include "./ui_MainWindow.h"

#include <lib/SnapshotsModel.h>
#include <lib/ScheduleModel.h>
#include "DiskForm.h"
#include <lib/DiskModel.h>
#include "WizardWindow.h"

#include <QtDBus>
#include <QFile>
#include <QStorageInfo>
#include <QLabel>
#include <QtDebug>

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , m_diskForm(new DiskForm(this))
    , ui(new Ui::MainWindow)
{
    ui->setupUi(this);

    ui->stackedWidget->addWidget(m_diskForm);

    ui->listView->setModel(new Backery::DiskModel(this));

    auto infos = QStorageInfo::mountedVolumes();

    auto last = std::unique(infos.begin(), infos.end(),[](const QStorageInfo &a, const QStorageInfo &b){
        return a.device()==b.device();
    });
    infos.erase(last, infos.end());

    for(const auto& info: qAsConst(infos)){
        if (!info.isValid() || !info.isReady() || info.isReadOnly() || QStringList({"tmpfs", "run"}).contains(info.fileSystemType())) {
            continue;
        }
        Backery::Disk disk(info);

        qobject_cast<Backery::DiskModel*>(ui->listView->model())->addDisk(disk);
    }


    if(!QDBusConnection::sessionBus().isConnected())
            qCritical() << "Cannot connect to the D-Bus session bus!";

    service = new org::manjaro::IBackeryService("org.manjaro.SBackeryService",
                                                "/org/manjaro/OBackeryService",
                                                QDBusConnection::sessionBus(), this);

    auto status = new QLabel(this);
    if( !service->isValid() )
        status->setText("<strong>Backery service is not started!</strong>");
    else
        status->setText("<strong>Backery service is working</strong>");
    ui->statusbar->addWidget(status);

    settings = new QSettings(QSettings::SystemScope, this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_pushButton_4_clicked()
{
    ui->stackedWidget->setCurrentIndex(2);
}


void MainWindow::on_listView_activated(const QModelIndex &index)
{
    if(index.row()!=-1){
        Backery::Disk newDisk = qobject_cast<Backery::DiskModel*>( ui->listView->model() ) -> diskAt(index.row());

        m_diskForm->setDisk( newDisk );

        if( settings->childGroups().contains(newDisk.uuid()) )
            ui->stackedWidget->setCurrentIndex(2);
        else
            ui->stackedWidget->setCurrentIndex(1);
    }
}


void MainWindow::on_actionWizard_triggered()
{
    auto wizard = new WizardWindow(this);
    wizard->setAttribute(Qt::WA_DeleteOnClose);
    wizard->show();
    auto wizardData = wizard->onFinishButtonClicked();
}

