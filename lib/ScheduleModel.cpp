#include "ScheduleModel.h"
#include <QDateTime>
namespace Backery {

ScheduleModel::ScheduleModel(QObject *parent) : QAbstractTableModel(parent)
{

}



QVariant ScheduleModel::data(const QModelIndex &index, int role) const
{
    if(role!=Qt::DisplayRole) return QVariant::Invalid;

    static QMap<Task::Type, QString> triggerIntervalName =
    {
        { Task::Type::Hourly, "hour"},
        {Task::Type::Daily, "day"},
        {Task::Type::Weekly, "week"},
        {Task::Type::Monthly, "month"},
        {Task::Type::Yearly, "year"}
    };

    switch (index.column()) {
    case 0:
        return m_tasks[index.row()].fsSnapshot?"Snapshot":"Backup";
    case 1:
        return QString("Every %1 %0(s)").arg(
                    triggerIntervalName[m_tasks[index.row()].triggerType]).arg(
                    m_tasks[index.row()].triggerStep);
    case 2:
        return QDateTime::currentDateTime().addSecs(60*60*m_tasks[index.row()].triggerStep);
    }
    return QVariant::Invalid;
}

QVariant ScheduleModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation!=Qt::Horizontal) return QVariant::Invalid;

    static const QList<QString> cols = {
        "Type", "Trigger", "Next"
    };
    if(role == Qt::DisplayRole){
        return cols[section];
    }
    return QVariant::Invalid;
}

}
