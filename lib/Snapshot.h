#ifndef SNAPSHOT_H
#define SNAPSHOT_H

#include <QDateTime>
#include <QString>
#include <QObject>

namespace Backery {
class Snapshot
{
    Q_GADGET
    Q_PROPERTY(QString systemName READ systemName WRITE setSystemName)
    Q_PROPERTY(QDateTime creationDate READ creationDate WRITE setCreationDate)
    Q_PROPERTY(QString comment READ comment WRITE setComment)
    Q_PROPERTY(QString type READ type)
public:
    Snapshot();

    virtual QString type() const{return "Unknown";};

    const QString &systemName() const;
    void setSystemName(const QString &newSystemName);

    const QDateTime &creationDate() const;
    void setCreationDate(const QDateTime &newCreationDate);

    const QString &comment() const;
    void setComment(const QString &newComment);

protected:
    QDateTime m_creationDate;

    QString m_systemName;
    QString m_comment;
};
}
#endif // SNAPSHOT_H
