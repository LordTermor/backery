#ifndef SNAPSHOTSMODEL_H
#define SNAPSHOTSMODEL_H
#include "Snapshot.h"

#include <QList>
#include <QAbstractTableModel>
#include <QMap>

#include <memory>

namespace Backery {

using SnapshotList = QList<std::shared_ptr<Snapshot>>;

class SnapshotsModel: public QAbstractTableModel
{
    Q_OBJECT
public:
    SnapshotsModel(QObject* parent = nullptr): QAbstractTableModel(parent){}

    void addSnapshot(std::shared_ptr<Snapshot> shot) {
        beginInsertRows(QModelIndex(), m_snapshots.size(), m_snapshots.size());
        m_snapshots.append(shot);
        endInsertRows();
    }
    std::shared_ptr<Snapshot> snapshot(uint64_t index) const {
        return m_snapshots[index];
    }
private:
    SnapshotList m_snapshots;

    struct ColumnData{
        QString displayName;
        QString property;
    };

    QList<ColumnData> m_columns = {
        {"Creation Date","creationDate"},
        {"System", "systemName"},
        {"Comment", "comment"},
        {"Type", "type"}
    };

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override{return m_snapshots.size();};
    virtual int columnCount(const QModelIndex &parent) const override{return m_columns.size();};
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};
}
#endif // SNAPSHOTSMODEL_H
