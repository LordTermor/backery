#pragma once

//Qt
#include <QAbstractListModel>

//C++
#include <filesystem>
#include <map>

//BTRFS
#include <btrfsutil.h>

class BtrfsSubvolumeModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit BtrfsSubvolumeModel(const std::filesystem::path& snapshot_path, QObject *parent = nullptr);

private:
    std::map<std::filesystem::path, std::unique_ptr<struct btrfs_util_subvolume_info>> m_subvolumes;

    // QAbstractItemModel interface
public:
    int rowCount(const QModelIndex &parent) const override;
    QVariant data(const QModelIndex &index, int role) const override;
};

