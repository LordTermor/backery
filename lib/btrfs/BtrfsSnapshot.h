#ifndef BACKERY_BTRFSSNAPHOT_H
#define BACKERY_BTRFSSNAPHOT_H

#include "FilesystemSnapshot.h"
#include <QList>
#include <QStringList>
#include <QFlags>

namespace Backery {

class BtrfsSnapshot : public FilesystemSnapshot
{
    Q_GADGET
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(QString uuid READ uuid WRITE setUuid)
    Q_PROPERTY(QString parentUuid READ parentUuid WRITE setParentUuid)
    Q_PROPERTY(QString receivedUuid READ receivedUuid WRITE setReceivedUuid)
    Q_PROPERTY(uint64_t subvolumeID READ subvolumeID WRITE setSubvolumeID)
    Q_PROPERTY(uint64_t generation READ generation WRITE setGeneration)
    Q_PROPERTY(uint64_t genAtCreation READ genAtCreation WRITE setGenAtCreation)
    Q_PROPERTY(QString parentID READ parentID WRITE setParentID)
    Q_PROPERTY(QString topLevelID READ topLevelID WRITE setTopLevelID)
    Q_PROPERTY(SnapshotFlags flags READ flags WRITE setFlags)
    Q_PROPERTY(QList<BtrfsSnapshot> snapshots READ snapshots WRITE setSnapshots)

public:
    enum BtrfsSnapshotFlags {
        Readonly = 1,
    };

    Q_DECLARE_FLAGS(SnapshotFlags, BtrfsSnapshotFlags)

    BtrfsSnapshot();
    virtual ~BtrfsSnapshot() = default;

    const QString &uuid() const;
    void setUuid(const QString &newUuid);

    const QString &parentUuid() const;
    void setParentUuid(const QString &newParentUuid);

    const QString &receivedUuid() const;
    void setReceivedUuid(const QString &newReceivedUuid);

    uint64_t subvolumeID() const;
    void setSubvolumeID(uint64_t newSubvolumeID);

    uint64_t generation() const;
    void setGeneration(uint64_t newGeneration);

    uint64_t genAtCreation() const;
    void setGenAtCreation(uint64_t newGenAtCreation);

    const QString &parentID() const;
    void setParentID(const QString &newParentID);

    const QString &topLevelID() const;
    void setTopLevelID(const QString &newTopLevelID);

    const QList<BtrfsSnapshot> &snapshots() const;
    void setSnapshots(const QList<BtrfsSnapshot> &newSnapshots);

    const SnapshotFlags &flags() const;
    void setFlags(const SnapshotFlags &newFlags);

    const QString &name() const;
    void setName(const QString &newName);

private:
    QString m_uuid;
    QString m_parentUuid;
    QString m_receivedUuid;
    uint64_t m_subvolumeID;
    uint64_t m_generation;
    uint64_t m_genAtCreation;
    QString m_parentID;
    QString m_topLevelID;
    QList<BtrfsSnapshot> m_snapshots;
    SnapshotFlags m_flags;
    QString m_name;
};

} // namespace Backery

Q_DECLARE_OPERATORS_FOR_FLAGS(Backery::BtrfsSnapshot::SnapshotFlags);


#endif // BACKERY_BTRFSSNAPHOT_H
