#include "BtrfsSnapshot.h"

namespace Backery {

BtrfsSnapshot::BtrfsSnapshot()
{

}

const QString &BtrfsSnapshot::uuid() const
{
    return m_uuid;
}

void BtrfsSnapshot::setUuid(const QString &newUuid)
{
    if (m_uuid == newUuid)
        return;

    m_uuid = newUuid;
}

const QString &BtrfsSnapshot::parentUuid() const
{
    return m_parentUuid;
}

void BtrfsSnapshot::setParentUuid(const QString &newParentUuid)
{
    if (m_parentUuid == newParentUuid)
        return;

    m_parentUuid = newParentUuid;
}

const QString &BtrfsSnapshot::receivedUuid() const
{
    return m_receivedUuid;
}

void BtrfsSnapshot::setReceivedUuid(const QString &newReceivedUuid)
{
    if (m_receivedUuid == newReceivedUuid)
        return;

    m_receivedUuid = newReceivedUuid;
}

uint64_t BtrfsSnapshot::subvolumeID() const
{
    return m_subvolumeID;
}

void BtrfsSnapshot::setSubvolumeID(uint64_t newSubvolumeID)
{
    if (m_subvolumeID == newSubvolumeID)
        return;

    m_subvolumeID = newSubvolumeID;
}

uint64_t BtrfsSnapshot::generation() const
{
    return m_generation;
}

void BtrfsSnapshot::setGeneration(uint64_t newGeneration)
{
    if (m_generation == newGeneration)
        return;

    m_generation = newGeneration;
}

size_t BtrfsSnapshot::genAtCreation() const
{
    return m_genAtCreation;
}

void BtrfsSnapshot::setGenAtCreation(uint64_t newGenAtCreation)
{
    if (m_genAtCreation == newGenAtCreation)
        return;

    m_genAtCreation = newGenAtCreation;
}

const QString &BtrfsSnapshot::parentID() const
{
    return m_parentID;
}

void BtrfsSnapshot::setParentID(const QString &newParentID)
{
    if (m_parentID == newParentID)
        return;

    m_parentID = newParentID;
}

const QString &BtrfsSnapshot::topLevelID() const
{
    return m_topLevelID;
}

void BtrfsSnapshot::setTopLevelID(const QString &newTopLevelID)
{
    if (m_topLevelID == newTopLevelID)
        return;

    m_topLevelID = newTopLevelID;
}

const QList<BtrfsSnapshot> &BtrfsSnapshot::snapshots() const
{
    return m_snapshots;
}

void BtrfsSnapshot::setSnapshots(const QList<BtrfsSnapshot> &newSnapshots)
{
    m_snapshots = newSnapshots;
}

const BtrfsSnapshot::SnapshotFlags &BtrfsSnapshot::flags() const
{
    return m_flags;
}

void BtrfsSnapshot::setFlags(const BtrfsSnapshot::SnapshotFlags &newFlags)
{
    if (m_flags == newFlags)
        return;

    m_flags = newFlags;
}

const QString &BtrfsSnapshot::name() const
{
    return m_name;
}

void BtrfsSnapshot::setName(const QString &newName)
{
    if (m_name == newName)
        return;

    m_name = newName;
}


} // namespace Backery
