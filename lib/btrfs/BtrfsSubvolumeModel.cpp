#include "BtrfsSubvolumeModel.h"

BtrfsSubvolumeModel::BtrfsSubvolumeModel(const std::filesystem::path &snapshot_path, QObject *parent): QAbstractListModel(parent)
{
    std::unique_ptr<struct btrfs_util_subvolume_iterator,
                    decltype (btrfs_util_destroy_subvolume_iterator)*>
        it{nullptr, &btrfs_util_destroy_subvolume_iterator};

    {
        btrfs_util_subvolume_iterator* itPtr;

        btrfs_util_create_subvolume_iterator(snapshot_path.c_str(), 0, 0, &itPtr);
        it.reset(itPtr);
    }

    auto info = std::make_unique<struct btrfs_util_subvolume_info>();
    char* pathPtr;

    while (btrfs_util_subvolume_iterator_next_info(it.get(), &pathPtr, info.get()) == BTRFS_UTIL_OK) {
        m_subvolumes.insert_or_assign(pathPtr, std::move(info));
        delete pathPtr;
        info = std::make_unique<struct btrfs_util_subvolume_info>();
    }
}

int BtrfsSubvolumeModel::rowCount(const QModelIndex &parent) const
{
    return m_subvolumes.size();
}

QVariant BtrfsSubvolumeModel::data(const QModelIndex &index, int role) const
{
    auto it = m_subvolumes.begin();
    std::advance(it, index.row());
    auto info = it->first;

    if(role==Qt::DisplayRole){
        return QString::fromStdString(info);
    }
    return QVariant::Invalid;
}
