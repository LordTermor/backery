#ifndef SCHEDULEMODEL_H
#define SCHEDULEMODEL_H

#include <QAbstractTableModel>
#include <QList>
namespace Backery {
struct Task{
    enum Type{
        Hourly,
        Daily,
        Weekly,
        Monthly,
        Yearly
    };

    Type triggerType;
    uint32_t triggerStep;
    bool fsSnapshot;

    bool operator==(const Task& other) const {
        return this->triggerType == other.triggerType
            && this->triggerStep == other.triggerStep
            && this->fsSnapshot == other.fsSnapshot;
    }
};
class ScheduleModel : public QAbstractTableModel
{
    Q_OBJECT
public:
    explicit ScheduleModel(QObject *parent = nullptr);



    void addTask(const Task& task){
        beginInsertRows(QModelIndex(), m_tasks.size(), m_tasks.size());
        m_tasks.append(task);
        endInsertRows();
    }

    const Task& taskAt(int i) const{
        return m_tasks[i];
    }

private:
    QList<Task> m_tasks;

    // QAbstractItemModel interface
public:
    virtual int rowCount(const QModelIndex &parent) const override
    {
        return m_tasks.size();
    }
    virtual int columnCount(const QModelIndex &parent) const override
    {
        return 3;
    }
    virtual QVariant data(const QModelIndex &index, int role) const override;
    virtual QVariant headerData(int section, Qt::Orientation orientation, int role) const override;
};

}
#endif // SCHEDULEMODEL_H
