#include "Snapshot.h"
namespace Backery {

Snapshot::Snapshot()
{

}

const QString &Snapshot::systemName() const
{
    return m_systemName;
}

void Snapshot::setSystemName(const QString &newSystemName)
{
    if (m_systemName == newSystemName)
        return;
    m_systemName = newSystemName;
}

const QDateTime &Snapshot::creationDate() const
{
    return m_creationDate;
}

void Snapshot::setCreationDate(const QDateTime &newCreationDate)
{
    if (m_creationDate == newCreationDate)
        return;
    m_creationDate = newCreationDate;
}

const QString &Snapshot::comment() const
{
    return m_comment;
}

void Snapshot::setComment(const QString &newComment)
{
    if (m_comment == newComment)
        return;
    m_comment = newComment;
}

}
