#include "ZfsSnapshot.h"

namespace Backery {

ZfsSnapshot::ZfsSnapshot()
{

}

const QString &ZfsSnapshot::syncPath() const
{
    return m_syncPath;
}

void ZfsSnapshot::setSyncPath(const QString &newSyncPath)
{
    if (m_syncPath == newSyncPath)
        return;

    m_syncPath = newSyncPath;
}

const QString &ZfsSnapshot::name() const
{
    return m_name;
}

void ZfsSnapshot::setName(const QString &newName)
{
    if (m_name == newName)
        return;

    m_name = newName;
}

const ZfsSnapshot::ZfsFlags &ZfsSnapshot::flags() const
{
    return m_flags;
}

void ZfsSnapshot::setFlags(const ZfsFlags &newFlags)
{
    m_flags = newFlags;
}

} // namespace Backery
