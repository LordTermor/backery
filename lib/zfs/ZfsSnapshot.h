#ifndef BACKERY_ZFSSNAPSHOT_H
#define BACKERY_ZFSSNAPSHOT_H

#include "FilesystemSnapshot.h"

namespace Backery {

class ZfsSnapshot : public FilesystemSnapshot
{
    Q_GADGET
    Q_PROPERTY(QString syncPath READ syncPath WRITE setSyncPath)
    Q_PROPERTY(QString name READ name WRITE setName)
    Q_PROPERTY(ZfsFlags flags READ flags WRITE setFlags)
public:
    ZfsSnapshot();

    enum ZfsSnapshotFlags {
        Compress = 1,
        Recursive = 2,
    };

    Q_DECLARE_FLAGS(ZfsFlags, ZfsSnapshotFlags)

    const QString &syncPath() const;
    void setSyncPath(const QString &newSyncPath);

    const QString &name() const;
    void setName(const QString &newName);

    const ZfsFlags &flags() const;
    void setFlags(const ZfsFlags &newFlags);

private:
    QString m_syncPath;
    QString m_name;
    ZfsFlags m_flags;
};

} // namespace Backery

#endif // BACKERY_ZFSSNAPSHOT_H
