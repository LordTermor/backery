#ifndef DISK_H
#define DISK_H
#include <QObject>
#include <QStorageInfo>

namespace Backery {

class Disk
{
    Q_GADGET
    Q_PROPERTY(QStorageInfo storageInfo READ storageInfo WRITE setStorageInfo)
    Q_PROPERTY(QString uuid READ uuid)
public:
    Disk(const QStorageInfo& info):m_storageInfo(info){ setUuid(); }
    Disk() = default;

    const QStorageInfo &storageInfo() const
    {
        return m_storageInfo;
    }
    void setStorageInfo(const QStorageInfo &newStorageInfo);

    const QString &uuid() const
    {
        return m_uuid;
    }
private:
    QString m_uuid;
    QStorageInfo m_storageInfo;

    void setUuid(){
        QStringList disks( QDir("/dev/disk/by-uuid/").entryList() );
        for( const QString &s : qAsConst(disks) ){
            if( QFile::symLinkTarget("/dev/disk/by-uuid/"+s) == m_storageInfo.device() ){
                m_uuid = s;
                break;
            }
        }
    }
};

}
#endif // DISK_H
