#include "DiskModel.h"

#include <QIcon>
namespace Backery {

DiskModel::DiskModel(QObject *parent) : QAbstractListModel(parent)
{

}
QVariant DiskModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case Qt::DisplayRole:{

        if(m_list[index.row()].storageInfo().name().length() == 0){
            return QString("%0 - %1")
                    .arg(QString(m_list[index.row()].storageInfo().device()))
                    .arg(QString(m_list[index.row()].storageInfo().fileSystemType()));
        } else {
            return QString("%0 (%1) - %2")
                    .arg(m_list[index.row()].storageInfo().name())
                    .arg(QString(m_list[index.row()].storageInfo().device()))
                    .arg(QString(m_list[index.row()].storageInfo().fileSystemType()));
        }

    }
    case Qt::DecorationRole:
        return QIcon::fromTheme("drive-harddisk-symbolic");
    }
    return QVariant::Invalid;
}

}
