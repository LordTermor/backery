#ifndef DISKMODEL_H
#define DISKMODEL_H

#include "Disk.h"

#include <QAbstractListModel>
#include <QObject>

namespace Backery {
using DiskList = QList<Disk>;
class DiskModel : public QAbstractListModel
{
    Q_OBJECT
public:
    explicit DiskModel(QObject *parent = nullptr);

    void addDisk(const Disk& newDisk){
        beginInsertRows(QModelIndex(), m_list.size(),m_list.size());
        m_list.append(newDisk);
        endInsertRows();
    }
    void removeDisk(int i) {
        if(i<0 && i>=m_list.size()) return;

        beginRemoveRows(QModelIndex(), i, i);
        m_list.removeAt(i);
        endRemoveRows();
    }
    const Disk& diskAt(int i) const{
        return m_list[i];
    }

private:
    DiskList m_list;

    // QAbstractItemModel interface
public:
    virtual int rowCount([[maybe_unused]]const QModelIndex &parent) const override
    {
        return m_list.size();
    }
    virtual QVariant data(const QModelIndex &index, int role) const override;
};

}

#endif // DISKMODEL_H
