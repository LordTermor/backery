#include "SnapshotsModel.h"
#include <QMetaProperty>
namespace Backery {


QVariant SnapshotsModel::data(const QModelIndex &index, int role) const
{
    switch (role) {
    case Qt::DisplayRole:{
        auto smo = Snapshot::staticMetaObject;
        auto propIndex = smo.indexOfProperty(m_columns[index.column()].property.toUtf8());
        auto snapshot = m_snapshots[index.row()];
        auto rowData = smo.property(propIndex).readOnGadget(snapshot.get());
        return rowData;
    }
    }
    return QVariant::Invalid;
}

QVariant SnapshotsModel::headerData(int section, Qt::Orientation orientation, int role) const
{
    if(orientation!=Qt::Horizontal){
        return QVariant::Invalid;
    }
    switch (role) {
    case Qt::DisplayRole:
        return m_columns[section].displayName;
    }
    return QVariant::Invalid;
}

}
